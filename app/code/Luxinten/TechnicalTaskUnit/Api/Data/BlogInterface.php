<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.10.2019
 * Time: 17:11
 */

namespace Luxinten\TechnicalTaskUnit\Api\Data;
/**
 * Interface BlogInterface
 * @package Luxinten\TechnicalTaskUnit\Api\Data
 */
interface BlogInterface
{
    const BLOG_ARTICLE_ID = 'article_id';
    const BLOG_ARTICLE_URL = 'article_url';
    const BLOG_ARTICLE_DESCRIPTION = 'article_description';
    const BLOG_ARTICLE_LANGUAGE = 'article_language';


    public function getArticleId();

    public function setArticleId($article_id);

    public function getArticleUrl();

    public function setArticleUrl($title);

    public function getArticleDescription();

    public function setArticleDescription($title);

    public function getArticleLanguage();

    public function setArticleLanguage($description);

}