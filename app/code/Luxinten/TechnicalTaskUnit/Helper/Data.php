<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.10.2019
 * Time: 22:49
 */

namespace Luxinten\TechnicalTaskUnit\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 * @package Luxinten\TechnicalTaskUnit\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    protected $_configInterface;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    private $_encryptor;

    const XML_PATH_LUXINTEN_SETTING_FOOTER_MESSAGE = 'luxinten_setting/luxinten_setting_general/luxinten_setting_footer_message';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $StoreManager
     * @param \Magento\Framework\Encryption\EncryptorInterface $Encryptor
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $StoreManager,
        \Magento\Framework\Encryption\EncryptorInterface $Encryptor
    )
    {
        $this->_configInterface = $configInterface;
        $this->_storeManager = $StoreManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $Encryptor;
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getFooterMessage($storeId = null)
    {
        $message = $this->_scopeConfig->getValue(
            self::XML_PATH_LUXINTEN_SETTING_FOOTER_MESSAGE,
            ScopeInterface::SCOPE_STORE,
            $storeId);
        return $this->decryptField($message);
    }

    /**
     * @param $field
     * @return mixed
     */
    private function decryptField($field)
    {
        return $this->_encryptor->decrypt($field);
    }
}