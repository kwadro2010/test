<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.10.2019
 * Time: 19:09
 */

namespace Luxinten\TechnicalTaskUnit\Plugin;

use Magento\Framework\View\Page\Title as TitleCore;

/**
 * Class PageTitle
 * @package Luxinten\TechnicalTaskUnit\Plugin
 */
class PageTitle
{
    const   TITLE_BEFORE_TEXT = "Test-";

    /**
     * @param TitleCore $subject
     * @param $title
     * @return string
     */
    public function beforeset(TitleCore $subject,$title)
    {

        if (strpos($title,self::TITLE_BEFORE_TEXT)===false){
            $title = self::TITLE_BEFORE_TEXT.$title;;
        }
        return $title;

    }
}