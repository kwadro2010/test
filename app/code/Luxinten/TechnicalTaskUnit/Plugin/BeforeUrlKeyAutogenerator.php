<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.10.2019
 * Time: 21:05
 */

namespace Luxinten\TechnicalTaskUnit\Plugin;

use Magento\CatalogUrlRewrite\Observer\ProductUrlKeyAutogeneratorObserver as BeforeUrlKeyObserver;

/**
 * Class BeforeSaveUrlRewrite
 * @package Luxinten\TechnicalTaskUnit\Plugin
 */
class BeforeUrlKeyAutogenerator
{
    const SUFFIX_URL = "_1";
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $_resource;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * BeforeSaveUrlRewrite constructor.
     * @param \Magento\Framework\App\ResourceConnection $Resource
     * @param \Magento\Store\Model\StoreManagerInterface $StoreManager
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $Resource,
        \Magento\Store\Model\StoreManagerInterface $StoreManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_resource = $Resource;
        $this->_messageManager = $messageManager;
        $this->_storeManager = $StoreManager;
        $this->_logger = $logger;
    }

    /**
     * @param RewriteSavingObserver $subject
     * @param null $result
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function afterexecute(BeforeUrlKeyObserver $subject, $result = NULL, \Magento\Framework\Event\Observer $observer)
    {
        $this->_logger->info('BeforeSaveUrlRewrite start');
        $_product = $observer->getEvent()->getProduct();
        $this->_logger->info('product_id :',(array)$_product->getId());
        if (!$_product->getId()) {
            $urlKey = $_product->getData('url_key');
            while (!$this->validateUrlKey($urlKey)) {
                $urlKey .= self::SUFFIX_URL;
            }
            $_product->setData('url_key', $urlKey);
        }
    }

    /**
     * @param $url_key
     * @return bool
     */
    private function validateUrlKey($url_key)
    {

        $connection = $this->getConnection();
        $tableUrlRewrite = $this->_resource->getTableName('url_rewrite');

        $request_path = $url_key . ".html";
        $select = $connection->select()->from(
            $tableUrlRewrite
        )->where('request_path=?', $request_path);
        if (count($connection->fetchAll($select)) > 0) {
            return false;
        }

        return true;

    }

    /**
     * @return mixed
     */
    private function getConnection()
    {
        return $this->_resource->getConnection();
    }

}
