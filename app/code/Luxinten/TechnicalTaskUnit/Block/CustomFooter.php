<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 25.10.2019
 * Time: 22:58
 */

namespace Luxinten\TechnicalTaskUnit\Block;
/**
 * Class CustomFooter
 * @package Luxinten\TechnicalTaskUnit\Block
 */
class CustomFooter extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Luxinten\TechnicalTaskUnit\Helper\Data
     */
    protected $_helper;

    /**
     * CustomFooter constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Luxinten\TechnicalTaskUnit\Helper\Data $Helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Luxinten\TechnicalTaskUnit\Helper\Data $Helper,
        array $data = []
    )
    {
        $this->_helper = $Helper;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getFooterMessage()
    {
        return $this->_helper->getFooterMessage();
    }
}