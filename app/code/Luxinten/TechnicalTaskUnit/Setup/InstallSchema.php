<?php

namespace Luxinten\TechnicalTaskUnit\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Luxinten\TechnicalTaskUnit\Api\Data\BlogInterface;

/**
 * Class InstallSchema
 * @package Luxinten\TechnicalTaskUnit\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('luxintent_blog_articles');
        // Check if the table already exists
        if (!$installer->tableExists($tableName)) {

            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    BlogInterface::BLOG_ARTICLE_ID,
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'BLOG ARTICLE ID'
                )
                ->addColumn(
                    BlogInterface::BLOG_ARTICLE_URL,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'BLOG ARTICLE URL'
                )
                ->addColumn(
                    BlogInterface::BLOG_ARTICLE_DESCRIPTION,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'BLOG ARTICLE DESCRIPTION'
                )
                ->addColumn(
                    BlogInterface::BLOG_ARTICLE_LANGUAGE,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'BLOG ARTICLE LANGUAGE CODE'
                )
                ->setComment('BLOG table ')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }

}