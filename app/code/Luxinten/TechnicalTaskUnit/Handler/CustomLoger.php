<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 26.10.2019
 * Time: 2:30
 */

namespace Luxinten\TechnicalTaskUnit\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

class CustomLoger extends Base
{
    protected $fileName = '/var/log/test/unit.log';
    protected $loggerType = Logger::DEBUG;
}

